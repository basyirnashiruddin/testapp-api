FROM maven:3.8-jdk-11

WORKDIR /usr/src/app

COPY . /usr/src/app
RUN mvn package -Dmaven.test.skip=true && cp target/*.jar app.jar

ENTRYPOINT ["java","-jar","app.jar"]
#ENV PORT 1945
#EXPOSE $PORT

#CMD ["sh", "-c", "mvn -Dserver.port=${PORT} spring-boot:run"]