package id.masbas.generalapis.controllers;

import id.masbas.generalapis.models.BaseResponse;
import id.masbas.generalapis.services.IIndexServices;
import id.masbas.generalapis.utils.ResponseUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;


@RestController
public class BaseController {

    @Autowired
    private IIndexServices indexServices;

    @GetMapping(value = "/")
    public BaseResponse getList() {
        return ResponseUtils.SetResponse(indexServices.findAll());
    }
}
