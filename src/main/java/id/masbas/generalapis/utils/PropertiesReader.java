package id.masbas.generalapis.utils;

import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.core.io.support.PropertiesLoaderUtils;

import java.io.IOException;
import java.util.Properties;

public class PropertiesReader {
    private static String PROPERTIES_PATH = "/";

    public static String ResponseProperties(String code, String msg) {
        Properties properties = OpenProperties("response.properties");
        return properties.getProperty("response.code" + code + ".message")
                .replaceAll("\\{msg}", msg);
    }
    private static Properties OpenProperties(String fileName) {
        Resource resource = new ClassPathResource(PROPERTIES_PATH + fileName);
        try {
            Properties prop = PropertiesLoaderUtils.loadProperties(resource);
            return prop;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }
}
