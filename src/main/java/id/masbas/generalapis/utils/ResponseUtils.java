package id.masbas.generalapis.utils;

import id.masbas.generalapis.models.BaseResponse;

public class ResponseUtils {
    public static String POST_RESPONSE_TYPE = "POST";
    public static String GET_RESPONSE_TYPE = "GET";
    public static String PUT_RESPONSE_TYPE = "PUT";

    public static BaseResponse SetResponse(Object data) {
        BaseResponse response = new BaseResponse(data);
        if (data==null) {
            response.setCode(400);
            response.setMessage(PropertiesReader.ResponseProperties("400","Data Kosong"));
        } else {
            response.setCode(200);
            response.setMessage(PropertiesReader.ResponseProperties("200",""));
        }
        return response;
    }
}
