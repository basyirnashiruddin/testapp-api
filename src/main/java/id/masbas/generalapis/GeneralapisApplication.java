package id.masbas.generalapis;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class GeneralapisApplication {

	public static void main(String[] args) {
		SpringApplication.run(GeneralapisApplication.class, args);
	}

}
