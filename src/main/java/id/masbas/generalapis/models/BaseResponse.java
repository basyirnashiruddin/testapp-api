package id.masbas.generalapis.models;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
public class BaseResponse {

    private Object data;
    private String message = "success";
    private int code = 200;

    public BaseResponse(Object data) {
        this.data = data;
    }
}
