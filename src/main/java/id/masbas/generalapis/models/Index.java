package id.masbas.generalapis.models;

import id.masbas.generalapis.utils.ResponseUtils;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@AllArgsConstructor
@Getter
@Setter
public class Index {

    private String id;
    private String name;
    private String path;
    // DEFAULT IS GET
    private String type = ResponseUtils.GET_RESPONSE_TYPE;

    public Index(String id, String name, String path) {
        this.id = id;
        this.name = name;
        this.path = path;
    }

}
