package id.masbas.generalapis.services;

import id.masbas.generalapis.models.Index;

import java.util.List;

public interface IIndexServices {
    List<Index> findAll();
}
