package id.masbas.generalapis.services;

import id.masbas.generalapis.models.Index;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class IndexServices implements IIndexServices {

    @Override
    public List<Index> findAll() {
        ArrayList<Index> indexs = new ArrayList<Index>();
        indexs.add(new Index("0","root","/"));
        indexs.add(new Index("1","home","/home"));
        indexs.add(new Index("2","about","/about"));
        return indexs;
    }
}
